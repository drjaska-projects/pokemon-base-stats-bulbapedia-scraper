#!/bin/bash -eu

rcut() {
	cat - | rev | cut "$@" | rev
}

for mon in $(find pokemons -type f | sort -V)
do
	# print mon name to file to begin with
	cat "$mon" | head -n 5 | tail -n 1 | cut -d '(' -f 1 | cut -d '>' -f 2 >> numbers.txt

	# find stat tables and possible headers
	grep -nE "<h[56]><span class=\"mw-headline\"|style=\"float:right\">[0-9]{1,3}<" "$mon" > preparse.txt

	cat preparse.txt | while read -r line
	do
		#echo "$line"
		if echo "$line" | grep -nEq "<h[56]><span class=\"mw-headline\""
		then
			#echo "matched header"
			echo "$line" | grep -nE "<h[56]><span class=\"mw-headline\"" | cut -d '>' -f 3 | cut -d '<' -f 1 >> numbers.txt

		elif echo "$line" | grep -nEq "style=\"float:right\">[0-9]{1,3}<"
		then
			#echo "matched table"
			echo "$line" | grep -nE "style=\"float:right\">[0-9]{1,3}<" | rcut -d "<" -f 2- | rcut -d ":" -f 1-3 | cut -d ">" -f 2- | sed -e 's#<.*>#\t#g' >> numbers.txt

		#else
			#echo "matched nothing???"
		fi
	done
done
