#!/bin/bash -eu

mkdir -p pokemons

#adjust this to first dex number to start scraping
i="1"

# update pokedex
#curl -L 'https://bulbapedia.bulbagarden.net/wiki/List_of_Pok%C3%A9mon_by_National_Pok%C3%A9dex_number' > pokedex.html

# find wiki urls for pokemons
for mon in $(grep -nrE "_\(Pok%C3%A9mon\)" pokedex.html | grep -v "srcset=" | grep -oe '<td><a href="/wiki/.*)" ' | grep -oe '/wiki/.*)' | awk '!a[$0]++')
do
	echo "$i: https://bulbapedia.bulbagarden.net$mon"
	curl -L "https://bulbapedia.bulbagarden.net$mon" > "pokemons/$i.html"
	((++i))

	# adjustable 'when to stop' number
	# ge: greater than or equal to
	if [ "$i" -ge "1337" ]
	then
		echo "i = $i, stopping due to limit"
		exit 0
	fi

	# be nice to their servers, pause for 1s to
	# distribute requests instead of spamming :)
	sleep 1
done
