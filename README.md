# Pokedex BST Scraper

Bulbapedia wiki scraping script for Pokemon base stats in vanilla games

Produces a numbers.txt file with Pokemons and their stats, all evolutions and regional forms

# Acknowledgements

The trailing space after the Pokemon's name is accidental, but I won't fix it. It's handy has a small separator to see when the Pokedex entries change as I have a text editor which very visibly highlights trailing whitespace. There are mega evolution names and images in an identical table to base stats and I couldn't find a neat way to parse them out with low effort.

# Motivation for this project

I couldn't find any place online which would have concisely listed all base stats of all Pokemons and their variants. The resulting numbers.txt was used in writing documentation for a Pokemon ROM hack.

# How to use

First run the bash shell script `scrape-pokedex.sh` to get the desired list of pokemons. After it run `parse-pokedex.sh`

# Dependencies

(GNU) coreutils, curl, sed

Project license: [WTFPL](https://en.wikipedia.org/wiki/WTFPL)
